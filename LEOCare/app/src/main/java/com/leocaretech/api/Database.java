package com.leocaretech.api;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.leocaretech.leocare.R;
import com.leocaretech.leocare.activities.VisitDetailActivity;

import java.util.ArrayList;

/**
 * Handles all connection to the database.
 *
 * @author Josh Wright
 */
public class Database {

    /**
     * A {@link String} containing the username to connect to the database.
     */
    private String username = null;

    /**
     * A {@link String} containing the password to connect to the database.
     */
    private String password = null;

    /**
     * A {@link Boolean} containing true/false depending if application is connected to the
     * database of not.
     */
    private Boolean isConnected = null;

    /**
     * A {@link SharedPreferences} object allowing me to store data in memory.
     */
    private SharedPreferences preferences = null;

    /**
     * Contains the current {@link Context}.
     */
    private Context context = null;

    /**
     * A parametrised constructor that takes in the required parameters and sets them on creation.
     *
     * @param username username to connect to the database
     * @param password password to connect to the database
     * @param context  context
     */
    public Database(String username, String password, Context context) {
        this.username = username;
        this.password = password;
        this.context = context;

        // Connect to the database - temporary hardcoded value used until access to the database.
        isConnected = true;

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Returns all visits relevant to a member of staff.
     *
     * @param staff member of staff to query the database with
     * @return      an {@link ArrayList} containing all visits in relation to a member of staff
     */
    public ArrayList<Visit> getVisits(Staff staff){

        //TODO : get all visits related to the staff member

        ArrayList<Visit> visits = new ArrayList<>();

        Staff s1 = new Staff("Josh Wright", context.getResources().getDrawable(R.drawable.gran),"07850251251", "Details");

        ArrayList<Activity> v1Activities = new ArrayList<>();
        v1Activities.add(new Activity("Help me take my Risperidone",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v1Activities.add(new Activity("Breakfast",context.getResources().getDrawable(R.drawable.meal), false,""));
        v1Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), false,""));
        v1Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), false,""));

        ArrayList<Activity> v2Activities = new ArrayList<>();
        v2Activities.add(new Activity("Help me take my warfarin",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v2Activities.add(new Activity("Lunch",context.getResources().getDrawable(R.drawable.meal), false,""));
        v2Activities.add(new Activity("Cup of Coffee",context.getResources().getDrawable(R.drawable.mug), false,""));
        v2Activities.add(new Activity("Help me get changed",context.getResources().getDrawable(R.drawable.red_cross), false,""));

        ArrayList<Activity> v3Activities = new ArrayList<>();
        v3Activities.add(new Activity("Help me wash",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v3Activities.add(new Activity("Dinner",context.getResources().getDrawable(R.drawable.meal), false,""));
        v3Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), false,""));
        v3Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), false,""));

        ArrayList<Activity> v4Activities = new ArrayList<>();
        v4Activities.add(new Activity("Breakfast",context.getResources().getDrawable(R.drawable.meal), false,""));
        v4Activities.add(new Activity("Help me take my Lorazepam",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v4Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v4Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), false,""));


        ServiceUser su1 = getServiceUsers().get(0);
        ServiceUser su2 = getServiceUsers().get(1);
        ServiceUser su3 = getServiceUsers().get(2);
        ServiceUser su4 = getServiceUsers().get(3);

        Visit v1 = new Visit(su1, s1, context.getResources().getDrawable(R.drawable.sun), "7am - 8am", "17th December 2016", v1Activities, "", false );
        Visit v2 = new Visit(su2, s1, context.getResources().getDrawable(R.drawable.sun), "9am - 10am", "17th December 2016", v1Activities, "", false );
        Visit v3 = new Visit(su3, s1, context.getResources().getDrawable(R.drawable.sun), "12pm - 2pm", "17th December 2016", v1Activities, "", false );
        Visit v4 = new Visit(su4, s1, context.getResources().getDrawable(R.drawable.sun), "4pm - 5pm", "17th December 2016", v1Activities, "", false );

        visits.add(v1);
        visits.add(v2);
        visits.add(v3);
        visits.add(v4);

        return visits;
    }


    /**
     * Returns all service users relevant to the care agency.
     *
     * @return      an {@link ArrayList} containing all service users
     */
    public ArrayList<ServiceUser> getServiceUsers(){

        // Diazepam, Mirtazapine, Lorazepam, Valproate Semisodium, Venlafaxine, Clozapine, Terbinafine,  Sinvastatin, Metformin, Aspirin, Risperidone, Zopiclone

        ArrayList<Medication> su1Meds = new ArrayList<>();
        su1Meds.add(new Medication("Warfarin", "details", true));
        su1Meds.add(new Medication("Diazepam", "details", true));
        su1Meds.add(new Medication("Mirtazapine", "details", true));
        su1Meds.add(new Medication("Lorazepam", "details", true));
        su1Meds.add(new Medication("Aspirin", "details", true));

        ArrayList<Medication> su2Meds = new ArrayList<>();
        su2Meds.add(new Medication("Lorazepam", "details", true));
        su2Meds.add(new Medication("Venlafaxine", "details", true));
        su2Meds.add(new Medication("Terbinafine", "details", true));
        su2Meds.add(new Medication("Warfarin", "details", true));
        su2Meds.add(new Medication("Aspirin", "details", true));
        su2Meds.add(new Medication("Mirtazapine", "details", true));

        ArrayList<Medication> su3Meds = new ArrayList<>();
        su3Meds.add(new Medication("Warfarin", "details", true));
        su3Meds.add(new Medication("Venlafaxine", "details", true));
        su3Meds.add(new Medication("Risperidone", "details", true));
        su3Meds.add(new Medication("Zopiclone", "details", true));

        ArrayList<Medication> su4Meds = new ArrayList<>();
        su4Meds.add(new Medication("Lorazepam", "details", true));
        su4Meds.add(new Medication("Diazepam", "details", true));
        su4Meds.add(new Medication("Venlafaxine", "details", true));
        su4Meds.add(new Medication("Warfarin", "details", true));
        su4Meds.add(new Medication("Mirtazapine", "details", true));

        ArrayList<ISPForm> su1ISP = new ArrayList<>();
        su1ISP.add(new ISPForm("form1", new ArrayList<String>()));
        su1ISP.add(new ISPForm("form2", new ArrayList<String>()));
        su1ISP.add(new ISPForm("form3", new ArrayList<String>()));

        // create service users

        ServiceUser su1 = new ServiceUser("Cheryl Smith",
                context.getResources().getDrawable(R.drawable.gran), "I want to remain living in my" +
                " own wonderful home.",
                "I am 74 years old and have lived on my own for the last 4 years since my husband " +
                        "john sadly passed away. I used to be a bingo caller and still enjoy a " +
                        "trip to the local bingo hall so as you can imagine I do and will enjoy a " +
                        "good natter with my support workers except when EastEnders is on the telly" +
                        ". My garden is only small but it's my pride and joy and as I am not as " +
                        "mobile as I used to be my support workers help me maintain it.",
                "I am allergic to peanuts, I suffer terribly with Hay Fever and I have always been a" +
                " little scared of dogs.", "Croydon", su1Meds, su1ISP);

        ServiceUser su2 = new ServiceUser("Robert Wright",
                context.getResources().getDrawable(R.drawable.gramp), "I want to be alive",
                "details about my life", "other details", "Brixton", su2Meds, su1ISP);

        ServiceUser su3 = new ServiceUser("Paul Rodgers",
                context.getResources().getDrawable(R.drawable.gramp_black), "I want to be alive",
                "details about my life", "other details", "Surrey", su3Meds, su1ISP);

        ServiceUser su4 = new ServiceUser("Eddie Mannion",
                context.getResources().getDrawable(R.drawable.gramp_white), "I want to be alive",
                "details about my life", "other details", "Croydon", su4Meds, su1ISP);

        ServiceUser su5 = new ServiceUser("James Roberts",
                context.getResources().getDrawable(R.drawable.su1), "I want to be alive",
                "details about my life", "other details", "Croydon", su2Meds, su1ISP);

        ServiceUser su6 = new ServiceUser("Corey Targett",
                context.getResources().getDrawable(R.drawable.su4), "I want to be alive",
                "details about my life", "other details", "Shoreditch", su1Meds, su1ISP);

        ServiceUser su7 = new ServiceUser("Leanne Woodward",
                context.getResources().getDrawable(R.drawable.su2), "I want to be alive",
                "details about my life", "other details", "West London", su1Meds, su1ISP);

        ServiceUser su8 = new ServiceUser("Charlotte Gammon",
                context.getResources().getDrawable(R.drawable.su3), "I want to be alive",
                "details about my life", "other details", "Kent", su4Meds, su1ISP);

        ArrayList<ServiceUser> sus = new ArrayList<>();
        sus.add(su1);
        sus.add(su2);
        sus.add(su3);
        sus.add(su4);
        sus.add(su5);
        sus.add(su6);
        sus.add(su7);
        sus.add(su8);

        return sus;
    }


    /**
     * Returns a list of all previous visits related to a member of staff.
     *
     * @param staff staff member to use as a query
     * @return      an {@link ArrayList} containing all visits related to a member of staff
     */
    public ArrayList<Visit> getPreviousVisits(Staff staff){


        ArrayList<Visit> visits = new ArrayList<>();

        Staff s1 = new Staff("Sharon Smith", context.getResources().getDrawable(R.drawable.carer1),"07850251251", "Details");
        Staff s2 = new Staff("Roberta Jones", context.getResources().getDrawable(R.drawable.carer2),"07850251251", "Details");
        Staff s3 = new Staff("Silvia Barton", context.getResources().getDrawable(R.drawable.carer3),"07850251251", "Details");
        Staff s4 = new Staff("Gazelle Mannion", context.getResources().getDrawable(R.drawable.carer4),"07850251251", "Details");

        ArrayList<Activity> v1Activities = new ArrayList<>();
        v1Activities.add(new Activity("Help me take my Risperidone",context.getResources().getDrawable(R.drawable.red_cross), true,""));
        v1Activities.add(new Activity("Breakfast",context.getResources().getDrawable(R.drawable.meal), true,""));
        v1Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), true,""));
        v1Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), true,""));

        ArrayList<Activity> v2Activities = new ArrayList<>();
        v2Activities.add(new Activity("Help me take my warfarin",context.getResources().getDrawable(R.drawable.red_cross), false,""));
        v2Activities.add(new Activity("Lunch",context.getResources().getDrawable(R.drawable.meal), true,""));
        v2Activities.add(new Activity("Cup of Coffee",context.getResources().getDrawable(R.drawable.mug), true,""));
        v2Activities.add(new Activity("Help me get changed",context.getResources().getDrawable(R.drawable.red_cross), true,""));

        ArrayList<Activity> v3Activities = new ArrayList<>();
        v3Activities.add(new Activity("Help me wash",context.getResources().getDrawable(R.drawable.red_cross), true,""));
        v3Activities.add(new Activity("Dinner",context.getResources().getDrawable(R.drawable.meal), true,""));
        v3Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), true,""));
        v3Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), true,""));

        ArrayList<Activity> v4Activities = new ArrayList<>();
        v4Activities.add(new Activity("Breakfast",context.getResources().getDrawable(R.drawable.meal), true,""));
        v4Activities.add(new Activity("Help me take my Lorazepam",context.getResources().getDrawable(R.drawable.red_cross), true,""));
        v4Activities.add(new Activity("Help me with my sudocream",context.getResources().getDrawable(R.drawable.red_cross), true,""));
        v4Activities.add(new Activity("Cup of tea",context.getResources().getDrawable(R.drawable.mug), true,""));

        ServiceUser su1 = getServiceUsers().get(0);
        ServiceUser su2 = getServiceUsers().get(1);
        ServiceUser su3 = getServiceUsers().get(2);
        ServiceUser su4 = getServiceUsers().get(3);

        Visit v1 = new Visit(su1, s1, context.getResources().getDrawable(R.drawable.sun), "7am - 8am", "17th December 2016", v1Activities, "", false );
        Visit v2 = new Visit(su2, s2, context.getResources().getDrawable(R.drawable.sun), "9am - 10am","18th December 2016", v2Activities, "", false );
        Visit v3 = new Visit(su3, s3, context.getResources().getDrawable(R.drawable.sun), "12pm - 2pm","20th December 2016", v3Activities, "", false );
        Visit v4 = new Visit(su4, s4, context.getResources().getDrawable(R.drawable.sun), "4pm - 5pm", "22th December 2016",v4Activities, "", false );
        Visit v5 = new Visit(su4, s4, context.getResources().getDrawable(R.drawable.sun), "1pm - 2pm", "25th December 2016",v1Activities, "", false );
        Visit v6 = new Visit(su4, s3, context.getResources().getDrawable(R.drawable.sun), "2pm - 5pm", "26th December 2016",v2Activities, "", false );
        Visit v7 = new Visit(su4, s1, context.getResources().getDrawable(R.drawable.sun), "8am - 10am", "30th December 2016",v1Activities, "", false );

        visits.add(v1);
        visits.add(v2);
        visits.add(v3);
        visits.add(v4);
        visits.add(v5);
        visits.add(v6);
        visits.add(v7);
        visits.add(v7);
        visits.add(v7);
        visits.add(v7);

        return visits;





    }

    /**
     * Returns all members of staff in the system.
     *
     * @return an {@link ArrayList} containing all members of staff on the system
     */
    public ArrayList<Staff> getStaff(){

        ArrayList<Staff> staff = new ArrayList<>();

        staff.add(new Staff("Josh Wright", context.getResources().getDrawable(R.drawable.carer1),"07850251251", "Details"));
        staff.add(new Staff("James Mannion", context.getResources().getDrawable(R.drawable.carer2),"07850251251", "Details"));
        staff.add(new Staff("David Barton", context.getResources().getDrawable(R.drawable.carer3),"07850251251", "Details"));
        staff.add(new Staff("Gazelle Mannion", context.getResources().getDrawable(R.drawable.carer4),"07850251251", "Details"));

        return staff;
    }

    /**
     * Allows a member of staff to clock in to a visit with a service user.
     *
     * @param staff member of staff to clock in
     * @param su    service user that the member of staff is visiting
     */
    public void clockIn(Staff staff, ServiceUser su){
        // TODO - clock in staff user to service user

        Intent i = new Intent(context, VisitDetailActivity.class);
        context.startActivity(i);
    }

    /**
     * Allows a carer to clock out of a visit.
     *
     * @param staff member of staff to clock on
     */
    public void clockOut(Staff staff){
        // TODO - complete implementation when we have access to the database
    }

    /**
     * Allows a user, carer or manager, to log in to the application via the server.
     *
     * @param usr username to query the database
     * @param pw  password to query the database
     * @return    a {@link Boolean} variable with result of attempted log in
     */
    public Boolean login(String usr, String pw){

        // TODO - connect to server and check log in details.

        if (usr.equals("josh")){

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("isManager", true);
            editor.commit();
        }
        return true;
    }

    /**
     * Allows a user, carer or manager, to log out to the application via the server.
     *
     * @param usr username to log out
     */
    public void logout(String usr){
        // TODO - complete implementation when we have access to the database
    }


}
