package com.leocaretech.api;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

public class Visit {

    /**
     * The {@link ServiceUser} involved with the visit.
     */
    private ServiceUser serviceUser = null;

    /**
     * The {@link Staff} involved with the visit.
     */
    private Staff staffMember = null;

    /**
     * A {@link Drawable} containing an image of the type of visit i.e. morning/evening.
     */
    private Drawable image = null;

    /**
     * A {@link String} containing the time of the visit.
     */
    private String time = null;

    /**
     * A {@link String} containing the date of the visit.
     */
    private String date = null;

    /**
     * An {@link ArrayList} containing all activities to complete during the visit.
     */
    private ArrayList<Activity> activities = null;

    /**
     * A {@link String} containing the care notes of the visit.
     */
    private String careNotes = null;

    /**
     * A {@link Boolean} containing a true/false value dependant on completion of visit.
     */
    private Boolean isComplete = null;

    /**
     * Constructor that takes in all required fields and sets them upon completion.
     *
     * @param su service user involved in the visit
     * @param staff member of staff involved in the visit
     * @param image type of visit
     * @param time time of the visit
     * @param date date of the visit
     * @param activities activities to complete within the visit
     * @param careNotes care notes regarding the visit
     * @param isComplete is the visit is complete or not
     */
    public Visit(ServiceUser su, Staff staff, Drawable image, String time, String date,
                 ArrayList<Activity> activities, String careNotes, Boolean isComplete){

        this.serviceUser = su;
        this.staffMember = staff;
        this.image = image;
        this.time = time;
        this.date = date;
        this.activities = activities;
        this.careNotes = careNotes;
        this.isComplete = isComplete;
    }

    /**
     * Allows a carer to complete the visit.
     */
    public void completeVisit(){
        //TODO : check is all activities have been complete
        //TODO : if they have
        isComplete = true;
    }

    /**
     * Allows a carer to add an ad-hoc activity to the visit.
     *
     * @param activity activity to add to the visit
     */
    public void addActivity(Activity activity){
        //TODO : add activity to the list
        this.activities.add(activity);
    }

    /**
     * Returns the date of the visit.
     *
     * @return A {@link String} containing the date of the visit
     */
    public String getDate() {
        return date;
    }

    /**
     * Returns the service user involved in the visit.
     *
     * @return a {@link ServiceUser} object containing all information regarding the service user
     * involved in the visit
     */
    public ServiceUser getServiceUser() {
        return serviceUser;
    }

    /**
     * Returns the staff member involved in the visit.
     *
     * @return a {@link Staff} object containing all information regarding the staff user
     * involved in the visit
     */
    public Staff getStaffMember() {
        return staffMember;
    }

    /**
     * Returns the image showing the type of visit.
     *
     * @return a {@link Drawable} containing an image to show the type of visit
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * Returns the time of the visit.
     *
     * @return A {@link String} containing the time of the visit
     */
    public String getTime() {
        return time;
    }

    /**
     * Returns a list of all activities to be completed during the visit.
     *
     * @return an {@link ArrayList} containing activities
     */
    public ArrayList<Activity> getActivities() {
        return activities;
    }

    /**
     * Returns the care notes of this specific visit.
     *
     * @return A {@link String} containing care notes
     */
    public String getCareNotes() {
        return careNotes;
    }

    /**
     * Returns a true or false depending on whether or not the visit has been completed.
     *
     * @return a {@link Boolean} containing true/false depending on completion status
     */
    public Boolean getComplete() {
        return isComplete;
    }
}
