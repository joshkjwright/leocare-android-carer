package com.leocaretech.api;

import android.graphics.drawable.Drawable;

/**
 * Represents an Activity.
 *
 * @author Josh Wright
 */
public class Activity {

    /**
     * A {@link String} containing the title of the activity.
     */
    private String title = null;

    /**
     * A {@link Drawable} containing the image relevant to the activity.
     */
    private Drawable image = null;

    /**
     * A {@link Boolean} containing true/false depending on status of completion.
     */
    private Boolean isComplete = null;

    /**
     * A {@link String} containing any notes relevant to the activity.
     */
    private String notes = null;

    /**
     * Parametrised constructor responsible for setting up the activity object. Takes in all
     * required fields in the parameters and sets the values.
     *
     * @param title         title of the activity
     * @param image         image related to the activity
     * @param isComplete    field containing value of completion
     * @param notes         care notes regarding the activity
     */
    public Activity(String title, Drawable image, Boolean isComplete, String notes) {
        this.title = title;
        this.image = image;
        this.isComplete = isComplete;
        this.notes = notes;
    }

    /**
     * This method sets the Activities isComplete Method to true.
     */
    public void complete(){
        this.isComplete = true;
    }

    /**
     * This method sets the Activities isComplete Method to false and notes the reason for such.
     *
     * @param reason reason the activity was incomplete
     */
    public void incomplete(String reason){
        this.notes = reason;
        this.isComplete = false;
    }


    /**
     * Returns the title of the activity.
     *
     * @return a {@link String} containing the title of the activity
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the activity.
     *
     * @param title the title of the activity
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the image related to the type of activity.
     *
     * @return a {@link Drawable} containing and image of the type of activity
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * Sets the image for the activity.
     *
     * @param image image related to the type of activity
     */
    public void setImage(Drawable image) {
        this.image = image;
    }

    /**
     * Returns {@link Boolean} field holding value dependant on whether or not the activity has been
     * completed.
     *
     * @return a {@link Boolean} containing true/false related to completion status
     */
    public Boolean getComplete() {
        return isComplete;
    }

    /**
     * Returns the notes regarding the activity. This field will usually hold details regarding the
     * activity if it hasn't been achieved.
     *
     * @return notes care notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the care notes regarding the activity. This field will usually hold details regarding
     * the activity if it hasn't been achieved.
     *
     * @param notes a {@link String} containing the care notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
