package com.leocaretech.api;

/**
 * Representation of a medication.
 *
 * @author Josh Wright
 */
public class Medication {

    /**
     * a {@link String} containing the name of the medication.
     */
    private String name = null;

    /**
     * a {@link String} containing all details regarding the medication.
     */
    private String details = null;

    /**
     * a {@link Boolean} containing true/false depending if the user is currently taking this
     * medicine or not.
     */
    private Boolean isCurrentlyTaking = null;

    /**
     * A constructor that takes in all required values and assigns the values on creation.
     *
     * @param name                  name of the medication
     * @param details               details regarding the medicine
     * @param isCurrentlyTaking     if the user is currently taking this medicine
     */
    public Medication(String name, String details, Boolean isCurrentlyTaking) {
        this.name = name;
        this.details = details;
        this.isCurrentlyTaking = isCurrentlyTaking;
    }

    /**
     * Returns the name of the medicine.
     *
     * @return a {@link String} containing the name of the medicine
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the details of the medicine.
     *
     * @return a {@link String} containing the details of the medicine
     */
    public String getDetails() {
        return details;
    }

    /**
     * Returns a true or false value depending on whether or not the user is currently taking this
     * medicine.
     *
     * @return a {@link Boolean} containing true/false depending on whether or not the user is
     * currently taking this medicine
     */
    public Boolean getCurrentlyTaking() {
        return isCurrentlyTaking;
    }
}
