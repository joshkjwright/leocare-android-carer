package com.leocaretech.api;

import android.graphics.drawable.Drawable;

/**
 * Represents an observation activity.
 *
 * @author Josh Wright
 */
public class ObservationActivity extends Activity {

    /**
     * A {@link String} containing the type of the observation.
     */
    private String type = null;

    /**
     * Constructor that takes in all required fields and sets them up on creation.
     *
     * @param title         title of the activity
     * @param image         image related to the type of activity
     * @param isComplete    a {@link Boolean} that contains true/false depending on completion
     *                      status
     * @param notes         care notes relating to this activity
     * @param type          type of observation
     */
    public ObservationActivity(String title, Drawable image, Boolean isComplete, String notes,
                               String type) {

        super(title, image, isComplete, notes);
        this.type = type;
    }

    /**
     * Returns the type of observation activity.
     *
     * @return a {@link String} containing the type of observation
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type of observation activity.
     *
     * @param type type of observation
     */
    public void setType(String type) {
        this.type = type;
    }
}
