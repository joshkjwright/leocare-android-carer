package com.leocaretech.api;

import android.graphics.drawable.Drawable;

/**
 * Representation of a staff member.
 *
 * @author Josh Wright
 */
public class Staff {

    /**
     * A {@link String} containing the name of the staff member.
     */
    private String name = null;

    /**
     * A {@link Drawable} containing the image of the staff member.
     */
    private Drawable image = null;

    /**
     * A {@link String} containing the number of the staff member.
     */
    private String number = null;

    /**
     * A {@link String} containing the details of the staff member.
     */
    private String details = null;

    /**
     * A constructor that takes in all required fields as parameters and sets the values on
     * creation.
     *
     * @param name name of the staff member
     * @param image image of the staff member
     * @param number mobile phone number of the staff member
     * @param details details regarding that member of staff
     */
    public Staff(String name, Drawable image, String number, String details){

        this.name = name;
        this.image = image;
        this.number = number;
        this.details = details;
    }

    /**
     * Returns the name of the staff member.
     *
     * @return a {@link String} containing the name of the staff member
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the staff member.
     *
     * @param name name of the staff member
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the image of the staff member.
     *
     * @return a {@link Drawable} containing the image of the staff member
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * Sets the image of the staff member.
     *
     * @param image image of the staff member in {@link Drawable} format
     */
    public void setImage(Drawable image) {
        this.image = image;
    }

    /**
     * Returns the mobile phone number of the staff member.
     *
     * @return a {@link String} containing the mobile phone number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the mobile phone number of a staff member.
     *
     * @param number mobile phone number
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Returns the details of a staff member.
     *
     * @return a {@link String} containing the details of the staff member
     */
    public String getDetails() {
        return details;
    }

    /**
     * Sets the details of the staff member.
     *
     * @param details personal details
     */
    public void setDetails(String details) {
        this.details = details;
    }
}
