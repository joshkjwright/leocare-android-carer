package com.leocaretech.api;

import android.graphics.drawable.Drawable;

/**
 * Representation of a medication activity.
 *
 * @author Josh Wright
 */
public class MedicationActivity extends Activity {

    /**
     * A {@link String} containing information on the medication.
     */
    private String form = null;

    /**
     * A {@link String} containing the route to take the medication.
     */
    private String route = null;

    /**
     * A {@link String} containing information regarding the dosage amount of the medication.
     */
    private String dosage = null;

    /**
     * A {@link String} containing the location in the service users house of the medication.
     */
    private String locationInHouse = null;

    /**
     * A constructor that takes in all required fields as parameters and sets them.
     *
     * @param title             title of the activity
     * @param image             image related to the type of activity
     * @param isComplete        {@link Boolean} containing true/false value based on completion status
     * @param notes             care notes regarding the activity
     * @param form              information regarding the form of the medication
     * @param route             information regarding how to take the medication
     * @param dosage            information regarding the dosage amount of the medication
     * @param locationInHouse   information regarding the location of the medication
     */
    public MedicationActivity(String title, Drawable image, Boolean isComplete, String notes,
                              String form, String route, String dosage, String locationInHouse) {

        super(title, image, isComplete, notes);
        this.form = form;
        this.route = route;
        this.dosage = dosage;
        this.locationInHouse = locationInHouse;
    }

    /**
     * Returns information regarding the form of the medication.
     *
     * @return a {@link String} containing the form of the medication
     */
    public String getForm() {
        return form;
    }

    /**
     * Sets the form of the medication.
     *
     * @param form form of the medication
     */
    public void setForm(String form) {
        this.form = form;
    }

    /**
     * Returns the route to take the medication.
     *
     * @return a {@link String} containing the route to take the medication
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets the route to take the medication.
     *
     * @param route how to take the medication
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Returns the dosage amount for the medication.
     *
     * @return a {@link String} containing the dosage of the medication
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * Sets the dosage of the medication.
     *
     * @param dosage dosage amount of the medication
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    /**
     * Returns the location of the medication in the service user's house.
     *
     * @return a {@link String} containing the location in the house
     */
    public String getLocationInHouse() {
        return locationInHouse;
    }

    /**
     * Sets the location of the medication within the service user's house.
     *
     * @param locationInHouse location of the medication
     */
    public void setLocationInHouse(String locationInHouse) {
        this.locationInHouse = locationInHouse;
    }
}
