package com.leocaretech.api;

import java.util.ArrayList;

/**
 * Represents an ISP Form
 *
 * @author Josh Wright
 */
public class ISPForm {

    /**
     * A {@link String} containing the name of the form.
     */
    private String name = null;

    /**
     * An {@link ArrayList} containing all the questions required to dynamically build the form.
     */
    private ArrayList<String> questions = null;

    /**
     * Constructor that takes in all required fields and sets them on creation.
     *
     * @param name      name of the form
     * @param questions questions within the form
     */
    public ISPForm(String name, ArrayList<String> questions) {

        this.name = name;
        this.questions = questions;
    }

    /**
     * Returns the name of the form.
     *
     * @return a {@link String} containing the name of the form
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the form.
     *
     * @param name name of the form
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the {@link ArrayList} of questions.
     *
     * @return an {@link ArrayList} containing all questions in the form
     */
    public ArrayList<String> getQuestions() {
        return questions;
    }

    /**
     * Sets the questions in the ISP form.
     *
     * @param questions questions of the form
     */
    public void setQuestions(ArrayList<String> questions) {
        this.questions = questions;
    }


}
