package com.leocaretech.api;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Represents a service user.
 *
 * @author Josh Wright
 */
public class ServiceUser {

    /**
     * A {@link String} containing the name of the service user.
     */
    private String name = null;

    /**
     * A {@link Drawable} containing an image of the service user.
     */
    private Drawable image = null;

    /**
     * A {@link String} containing the outcome of the service user.
     */
    private String outcome = null;

    /**
     * A {@link String} containing a short biography of the service user.
     */
    private String myLife = null;

    /**
     * A {@link String} containing other details regarding the service user.
     */
    private String otherDetails = null;

    /**
     * A {@link String} containing the address of the service user.
     */
    private String address = null;

    /**
     * An {@link ArrayList} containing all the medication the service user is currently taking.
     */
    private ArrayList<Medication> medications = null;

    /**
     * An {@link ArrayList} containing all the relevant ISP forms for the service user.
     */
    private ArrayList<ISPForm> ispForms = null;

    /**\
     * A constructor that takes in all required fields as parameters and sets the values on creation.
     *
     * @param name          service user's name
     * @param image         service user's image
     * @param outcome       service user's primary outcome
     * @param myLife        biography
     * @param otherDetails  other details
     * @param address       current residency
     * @param medications   medication history
     * @param ispForms      current ISP forms
     */
    public ServiceUser(String name, Drawable image, String outcome, String myLife,
                       String otherDetails, String address, ArrayList<Medication> medications,
                       ArrayList<ISPForm> ispForms) {

        this.name = name;
        this.image = image;
        this.outcome = outcome;
        this.myLife = myLife;
        this.otherDetails = otherDetails;
        this.address = address;
        this.medications = medications;
        this.ispForms = ispForms;
    }

    /**
     * Returns the current residency of the service user.
     *
     * @return a {@link String} containing the current residency of the service user
     */
    public String getAddress() {
        return address;
    }

    /**
     * Returns the service user's name.
     *
     * @return a {@link String} containing the service user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the image of the service user.
     *
     * @return an {@link Drawable} image of the service user
     */
    public Drawable getImage() {
        return image;
    }

    /**
     * Returns the service user's outcome.
     *
     * @return a {@link String} containing the service user's outcome
     */
    public String getOutcome() {
        return outcome;
    }

    /**
     * Returns the service user's short biography.
     *
     * @return a {@link String} containing the service user's biography
     */
    public String getMyLife() {
        return myLife;
    }

    /**
     * Returns other details regarding the service user.
     *
     * @return a {@link String} containing other details regarding the service user
     */
    public String getOtherDetails() {
        return otherDetails;
    }

    /**
     * Returns a list of all medication in relation to the service user.
     *
     * @return an {@link ArrayList} of all medication in relation to the service user
     */
    public ArrayList<Medication> getMedications() {
        return medications;
    }

    /**
     * Returns all ISP forms in relation to the service user.
     *
     * @return an {@link ArrayList} of all ISP forms in relation to the service user
     */
    public ArrayList<ISPForm> getIspForms() {
        return ispForms;
    }
}
