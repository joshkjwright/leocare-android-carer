package com.leocaretech.leocare.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.leocaretech.leocare.CaptureSignatureView;
import com.leocaretech.leocare.NavDrawer;
import com.leocaretech.leocare.R;

/**
 * Handles the completion of an {@link com.leocaretech.api.Activity}, allows the user to draw
 * their signature to sign off the visit.
 *
 * @author Josh Wright
 */
public class CompleteVisitActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * Button that allows a user to progress onto the next screen.
     */
    private Button bCompleteVisit = null;

    /**
     * This method is responsible for setting up the screen and fields on creation.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_visit);

        // Sets up all the fields
        this.setupFields();
    }

    /**
     * Sets up all the fields on this screen
     */
    private void setupFields(){
        bCompleteVisit = (Button) findViewById(R.id.bCompleteVisitFinal);
        bCompleteVisit.setOnClickListener(this);

        // Signature Capture
        LinearLayout signatureCapture = (LinearLayout) findViewById(R.id.captureSignatureCompleteVisit);
        CaptureSignatureView mSig = new CaptureSignatureView(this, null);
        signatureCapture.addView(mSig, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
    }

    /**
     * Handles all on click events from this screen.
     *
     * @param view view from which the on click event originated from
     */
    @Override
    public void onClick(View view) {

        // TODO: save signature to device/push signature to server using the getBytes method.

        // Back to main navigation screen once visit has been completed.
        Intent i = new Intent(getBaseContext(), NavDrawer.class);
        startActivity(i);
    }
}
