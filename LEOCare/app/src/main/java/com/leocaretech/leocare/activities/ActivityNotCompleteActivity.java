package com.leocaretech.leocare.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.leocaretech.leocare.R;

/**
 * Handles an incomplete {@link com.leocaretech.api.Activity}
 */
public class ActivityNotCompleteActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Button allowing the user to submit the care notes regarding the incomplete
     * {@link com.leocaretech.api.Activity}
     */
    private Button bSubmit = null;

    /**
     * Method called on creation of the screen, responsible for setting up the fields.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_complete);

        // Set up fields
        setupFields();
    }

    // Set up fields
    private void setupFields(){

        this.bSubmit = (Button) findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(this);
    }

    /**
     * Handles all on click events originating from this screen.
     *
     * @param v view from which the click event originated from
     */
    @Override
    public void onClick(View v) {

        // TODO - send this information to the server
        super.onBackPressed();
        super.onBackPressed();
    }
}
