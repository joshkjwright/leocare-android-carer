package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.Visit;
import com.leocaretech.leocare.R;

import java.util.ArrayList;

/**
 * Handles the creation of a custom listview containing {@link Visit}s.
 *
 * @author Josh Wright
 */
public class VisitListViewAdapter extends BaseAdapter {

    /**
     * Inflater to inflate the listview.
     */
    private static LayoutInflater inflater = null;

    /**
     * Database object to ensure safe access to the database.
     */
    private Database db = null;

    /**
     * Inflates the listview using the required context parameter.
     *
     * @param context context of parent class
     */
    public VisitListViewAdapter(Context context){

        inflater = ( LayoutInflater ) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new Database("x", "y", context);
    }

    /**
     * Returns the total count of listview cells.
     *
     * @return amount of cells in the custom listview
     */
    @Override
    public int getCount() {
        return db.getVisits(db.getStaff().get(0)).size();
    }

    /**
     * Get item at a specific position.
     *
     * @param position position of item
     * @return position of item
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Returns the id of the requested item.
     *
     * @param position position of the request item
     * @return position of the item
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Generates the custom listview cell.
     *
     * @param position position of the cell in the listview
     * @param convertView view to convert into new view
     * @param parent parent view
     * @return newly created view to insert into the custom listview
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_visits_cell, null);

        // Get fields
        ImageView image = (ImageView) rowView.findViewById(R.id.imageVisitClientImage);
        TextView tName = (TextView) rowView.findViewById(R.id.textVisitClientName);
        TextView tIllness = (TextView) rowView.findViewById(R.id.textVisitClientIllness);
        TextView tTime = (TextView) rowView.findViewById(R.id.tvVisitDetailTime);

        // Get visits
        ArrayList<Visit> visits = db.getVisits(db.getStaff().get(0));

        // Get specific visit using position in the listview
        Visit visit = visits.get(position);

        // Set the fields
        image.setImageDrawable(visit.getServiceUser().getImage());
        tName.setText(visit.getServiceUser().getName());
        tIllness.setText("Type 2 Diabetes");
        tTime.setText(visit.getTime());

        // return the view
        return rowView;
    }



}
