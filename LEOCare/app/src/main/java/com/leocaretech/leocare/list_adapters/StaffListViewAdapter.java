package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.leocare.R;

import java.util.ArrayList;

/**
 * Handles the creation of a custom listview containing {@link Staff} members.
 *
 * @author Josh Wright
 */
public class StaffListViewAdapter extends BaseAdapter {

    /**
     * Inflater to inflate the listview.
     */
    private static LayoutInflater inflater = null;

    /**
     * Array of members of staff to populate the listview with.
     */
    private ArrayList<Staff> staff = null;

    /**
     * Inflates the listview using the required context parameter.
     *
     * @param context context of parent class
     */
    public StaffListViewAdapter(Context context){

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get the members of staff
        Database db = new Database("x", "y", context);
        staff = db.getStaff();
    }

    /**
     * Returns the total count of listview cells.
     *
     * @return amount of cells in the custom listview
     */
    @Override
    public int getCount() {
        return this.staff.size();
    }

    /**
     * Get item at a specific position.
     *
     * @param position position of item
     * @return position of item
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Returns the id of the requested item.
     *
     * @param position position of the request item
     * @return position of the item
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Generates the custom listview cell.
     *
     * @param position position of the cell in the listview
     * @param convertView view to convert into new view
     * @param parent parent view
     * @return newly created view to insert into the custom listview
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_staff_cell, null);

        // Get the fields
        TextView tvStaffName = (TextView) rowView.findViewById(R.id.tvStaffName);
        ImageView ivStaffImage = (ImageView) rowView.findViewById(R.id.ivStaffPicture);

        // Set the fields
        tvStaffName.setText(this.staff.get(position).getName());
        ivStaffImage.setImageDrawable(this.staff.get(position).getImage());

        // Return the view
        return rowView;
    }
}
