package com.leocaretech.leocare.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.ServiceUser;
import com.leocaretech.api.Staff;
import com.leocaretech.api.Visit;
import com.leocaretech.leocare.R;

/**
 * Handles the start of a visit, including electronic call monitoring and displaying directions
 * to the service user's residence.
 *
 * @author Josh Wright
 */
public class StartVisitActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * Button to show directions to the service user's residence.
     */
    private Button bStartDirections = null;

    /**
     * Button to enable carers to clock in to the visit.
     */
    private Button bEMC = null;

    /**
     * Database object to allow controlled access to the database.
     */
    private Database db = null;

    /**
     * Textview containing the title of the visit.
     */
    private TextView tvTitle = null;

    /**
     * Textview containing the time of the visit.
     */
    private TextView tvTime = null;

    /**
     * Imageview containing the image of the visit.
     */
    private ImageView ivImage = null;

    /**
     * Textview containing any updates regarding the service user.
     */
    private TextView tvUpdates = null;

    /**
     * On create method to handle activity creation and field set up.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_visit_actvity);

        // get shared prefs
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        tvTitle = (TextView) findViewById(R.id.tvStartVisitTitle);
        tvTime = (TextView) findViewById(R.id.tvStartVisitTime);
        ivImage = (ImageView) findViewById(R.id.ivStartVisitImage);
        tvUpdates = (TextView) findViewById(R.id.tvStartVisitUpdates);

        this.db = new Database("","", this);

        Visit v = db.getVisits(db.getStaff().get(0)).get(preferences.getInt("visitID", 0));

        // set title, time and any updates
        tvTitle.setText("Your morning visit with " + v.getServiceUser().getName());
        tvTime.setText(v.getTime());
        tvUpdates.setText("There are no recent updates for " + v.getServiceUser().getName());

        // set up buttons
        bStartDirections = (Button) findViewById(R.id.bStartDirections);
        bEMC = (Button) findViewById(R.id.bECM);
        bStartDirections.setOnClickListener(this);
        bEMC.setOnClickListener(this);

    }

    /**
     * Handle any on click event.
     *
     * @param view view from which the event came
     */
    @Override
    public void onClick(View view) {

        switch(view.getId()){

            case R.id.bStartDirections:

                // TODO - make dynamic - pull address from database.

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q=Gloucester"));
                startActivity(intent);

                break;

            case R.id.bECM:

                db.clockIn(db.getStaff().get(0), db.getServiceUsers().get(0));

                break;

        }

    }
}
