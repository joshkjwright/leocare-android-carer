package com.leocaretech.leocare.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.leocaretech.api.Database;
import com.leocaretech.leocare.NavDrawer;
import com.leocaretech.leocare.R;

/**
 * Handles the log in screen and allows the user to log in to the application.
 *
 * @author Josh Wright
 */
public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Button that allows user to log in.
     */
    private Button bLogin = null;

    /**
     * EditText that contains the users email/username.
     */
    private EditText etEmail;

    /**
     * Shared Preferences object that enables data to be saved in memory.
     */
    private SharedPreferences preferences;

    /**
     * Database object that allows controlled access to the database.
     */
    private Database db = null;

    /**
     * On create method that handles the set up of the screen and all fields within.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        // Get shared preferences
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Initialise the database
        this.db = new Database("x", "y", this);

        // Reset manager shared pref value
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isManager", false);
        editor.commit();

        // Set up fields
        this.setupFields();
    }

    /**
     * Handles the set up of all fields within the screen.
     */
    private void setupFields(){

        bLogin = (Button) findViewById(R.id.buttonLogIn);
        bLogin.setOnClickListener(this);
        etEmail = (EditText) findViewById(R.id.etEmail);
    }

    /**
     * Handles on click events from this screen.
     *
     * @param v view from which the on click originated from
     */
    @Override
    public void onClick(View v) {

        if (db.login(etEmail.getText().toString(), "")){

            // Allow access to the app
            Intent intent = new Intent(this, NavDrawer.class);
            startActivity(intent);

        } else {
            // Display if username/password is incorrect
            Toast.makeText(this, "Username or Password incorrect", Toast.LENGTH_LONG).show();
        }
    }
}
