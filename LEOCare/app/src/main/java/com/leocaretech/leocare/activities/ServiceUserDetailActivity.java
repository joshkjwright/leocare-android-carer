package com.leocaretech.leocare.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.leocaretech.api.Database;
import com.leocaretech.leocare.R;
import com.leocaretech.leocare.fragments.service_user_fragments.ServiceUserCareNotesFragment;
import com.leocaretech.leocare.fragments.service_user_fragments.ServiceUserISPFragment;
import com.leocaretech.leocare.fragments.service_user_fragments.ServiceUserMedicationFragment;
import com.leocaretech.leocare.fragments.service_user_fragments.ServiceUserMyLifeFragment;

/**
 * Handles navigation to all screens in relation to the service user i.e. details, medical records,
 * ISP forms, previous visits and any updates.
 *
 * @author Josh Wright
 */
public class ServiceUserDetailActivity extends AppCompatActivity {

    /**
     * Handles all tabs within the screen.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * Handles all tabes within the screen.
     */
    private ViewPager mViewPager;

    /**
     * Handles screen creation and field set up.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_user_detail);

        // Set up tabs
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Database db = new Database("x", "y", this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        // Set title of Activity to name of the correct service user
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(db.getServiceUsers().get(preferences.getInt("serviceUserID", 0)).getName());
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        /**
         * Creates the SectionsPagerAdapter.
         *
         * @param fm managers all fragments within the sections pager adapter
         */
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Handles fragment management to ensure correct screens are shown.
         *
         * @param position position within the sections pager adapter
         * @return the corresponding fragment
         */
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return new StaffFragment();

            switch(position){
                case 0:
                    // ABOUT ME
                    return new ServiceUserMyLifeFragment();
                case 1:
                    // MEDICATION
                    return new ServiceUserMedicationFragment();
                case 2:
                    // ISP
                    return new ServiceUserISPFragment();
                case 3:
                    // CARE NOTES
                    return new ServiceUserCareNotesFragment();
                case 4:
                    // UPDATES
                    // TODO - create this screen.
                default:
                    return new ServiceUserMyLifeFragment();
            }
        }

        /**
         * Returns the count of sections/tabs.
         *
         * @return number of sections
         */
        @Override
        public int getCount() {
            // Show 5 total pages.
            return 5;
        }

        /**
         * Handles the changing of page title.
         *
         * @param position position within the section adapter
         * @return title of the current section
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "My Life";
                case 1:
                    return "Medication Records";
                case 2:
                    return "ISP";
                case 3:
                    return "Visits";
                case 4:
                    return "Updates";
            }
            return null;
        }
    }
}
