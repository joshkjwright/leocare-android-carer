package com.leocaretech.leocare.fragments.service_user_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.leocaretech.leocare.R;
import com.leocaretech.leocare.list_adapters.ServiceUserISPListViewAdapter;

import java.util.ArrayList;

/**
 * Displays all {@link com.leocaretech.api.ISPForm]s relevant to a specific
 * {@link com.leocaretech.api.ServiceUser}.
 *
 * @author Josh Wright
 */
public class ServiceUserISPFragment extends Fragment {

    /**
     * Empty default constructor required for fragments.
     */
    public ServiceUserISPFragment(){}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_user_isp, container, false);

        // set up fields
        this.setupListView(rootView);

        return rootView;
    }

    /**
     * Set up views.
     *
     * @param rootView context of the rootview
     */
    private void setupListView(View rootView){

        ListView lvISP = (ListView) rootView.findViewById(R.id.lvServiceUserISP);
        lvISP.setAdapter(new ServiceUserISPListViewAdapter(getContext()));
    }
}
