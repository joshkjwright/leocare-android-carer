package com.leocaretech.leocare.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.api.Visit;
import com.leocaretech.leocare.R;
import com.leocaretech.leocare.list_adapters.ActivityListViewAdapter;

/**
 * Shows all details regarding a specific {@link Visit}.
 *
 * @author Josh Wright
 */
public class VisitDetailActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener{

    /**
     * A listview containing all the activities to complete during the visit.
     */
    private ListView activitiesListView = null;

    /**
     * Shared Prefs to allow data to be stored in system memory.
     */
    private SharedPreferences preferences = null;

    /**
     * Button to allow users to complete a visit.
     */
    private Button bCompleteVisit = null;

    /**
     * Textview containing the title of the visit.
     */
    private TextView tvTitle = null;

    /**
     * Textview containing the time of the visit.
     */
    private TextView tvTime = null;

    /**
     * Imageview containing type of visit in image form.
     */
    private ImageView ivImage = null;

    /**
     * Handles activity creation and field set up.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_detail);

        // Set up fields
        this.setupFields();
    }

    // Set up fields
    private void setupFields(){

        // Get shared preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        tvTitle = (TextView) findViewById(R.id.tvVisitDetailTitle);
        tvTime = (TextView) findViewById(R.id.tvVisitDetailTime);
        ivImage = (ImageView) findViewById(R.id.ivVisitDetailImage);

        Database db = new Database("","", this);

        // Get the correct Visit
        Visit v = db.getVisits(db.getStaff().get(0)).get(preferences.getInt("visitID", 0));

        // Set the title text and time
        tvTitle.setText("Your morning visit with " + v.getServiceUser().getName());
        tvTime.setText(v.getTime());

        // TODO - set image depending on what time of day

        // Set up complete visit button
        bCompleteVisit = (Button) findViewById(R.id.bCompleteVisit);
        bCompleteVisit.setOnClickListener(this);

        // Set up listview
        this.activitiesListView = (ListView) findViewById(R.id.activitiesListView);
        this.activitiesListView.setAdapter(new ActivityListViewAdapter(this));
        this.activitiesListView.setOnItemClickListener(this);
    }

    /**
     * Create the menu to display option to add ad-hoc {@link com.leocaretech.api.Activity} to the
     * {@link Visit}.
     *
     * @param menu menu object to use with the inflator
     * @return true/false depending on creation
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visit_detail, menu);
        return true;
    }

    /**
     * Handles menu item selection.
     *
     * @param item menu item selected.
     * @return true/false depending on handled or not
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        // TODO - launch add ad-hoc activity code

        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is called on every return to this activity to check whether all activities have
     * been complete and to handle the 'complete visit' button's visibility.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(preferences.getBoolean("complete", false)){
            bCompleteVisit.setVisibility(View.VISIBLE);
        } else {
            bCompleteVisit.setVisibility(View.GONE);
        }

    }

    /**
     * Handles on item click events coming from the list of activities.
     *
     * @param parent parent view
     * @param view current view
     * @param position position in listview
     * @param l the row id of the item that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

        Intent i = new Intent(parent.getContext(), ActivityDetailActivity.class);
        i.putExtra("pos", position);
        startActivity(i);
    }

    /**
     * Handles on click events coming from the 'complete visit' button.
     *
     * @param view view from which click event originated from
     */
    @Override
    public void onClick(View view) {

        Intent i = new Intent(getBaseContext(), CompleteVisitActivity.class);
        startActivity(i);

    }
}
