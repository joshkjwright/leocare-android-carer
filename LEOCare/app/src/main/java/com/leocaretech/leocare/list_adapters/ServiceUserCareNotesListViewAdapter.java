package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Activity;
import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.api.Visit;
import com.leocaretech.leocare.R;

import java.util.ArrayList;

/**
 * Handles the creation of a custom listview containing care notes regarding previous
 * {@link Visit}s.
 *
 * @author Josh Wright
 */
public class ServiceUserCareNotesListViewAdapter extends BaseAdapter {

    /**
     * Inflater to inflate the listview.
     */
    private static LayoutInflater inflater = null;

    /**
     * Arraylist of previous visits to populate the listview with.
     */
    private ArrayList<Visit> visits = null;

    /**
     * Inflates the listview using the required context parameter, also pulls all information
     * regarding previous visits from the server to use to populate the listview.
     *
     * @param context context of parent class
     */
    public ServiceUserCareNotesListViewAdapter(Context context){

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Database db = new Database("x", "y", context);
        this.visits = db.getPreviousVisits(db.getStaff().get(0));
    }

    /**
     * Returns the total count of listview cells.
     *
     * @return amount of cells in the custom listview
     */
    @Override
    public int getCount() {
        return this.visits.size();
    }

    /**
     * Get item at a specific position.
     *
     * @param position position of item
     * @return position of item
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Returns the id of the requested item.
     *
     * @param position position of the request item
     * @return position of the item
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Generates the custom listview cell.
     *
     * @param position position of the cell in the listview
     * @param convertView view to convert into new view
     * @param parent parent view
     * @return newly created view to insert into the custom listview
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_service_user_care_notes_cell, null);

        // get fields
        TextView tvStaffName = (TextView) rowView.findViewById(R.id.tvServiceUserCareNotesListViewStaffName);
        ImageView tvStaffImage = (ImageView) rowView.findViewById(R.id.ivServiceUserCareNotesListViewStaffPicture);
        TextView tvTime = (TextView) rowView.findViewById(R.id.tvServiceUserCareNotesListViewTime);
        TextView tvDate = (TextView) rowView.findViewById(R.id.tvServiceUserCareNotesListViewDate);
        TextView tvAlert = (TextView) rowView.findViewById(R.id.tvServiceUserCareNotesListViewAlertText);
        ImageView ivAlert = (ImageView) rowView.findViewById(R.id.ivServiceUserCareNotesListViewAlert);

        // get the current visit at the position of the listview
        Visit v = visits.get(position);

        // set the fields
        tvStaffName.setText(v.getStaffMember().getName());
        tvStaffImage.setImageDrawable(v.getStaffMember().getImage());
        tvTime.setText(v.getTime());
        tvDate.setText(v.getDate());

        // calculate total alerts to display in the listview
        int totalIncomplete = 0;

        for (Activity a : v.getActivities()){
            if (!a.getComplete()){
                totalIncomplete++;
            }
        }

        // display the alerts
        if (totalIncomplete != 0){
            tvAlert.setText(totalIncomplete + " Alert");
        } else {
            tvAlert.setText(" Achieved");
            ivAlert.setImageDrawable(rowView.getResources().getDrawable(R.drawable.green_tick));
        }

        // return the view
        return rowView;
    }
}

