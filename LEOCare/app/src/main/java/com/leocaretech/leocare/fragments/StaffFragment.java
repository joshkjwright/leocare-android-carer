package com.leocaretech.leocare.fragments;

import android.content.Intent;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.leocaretech.leocare.R;
import com.leocaretech.leocare.activities.StaffDetailActivity;
import com.leocaretech.leocare.list_adapters.StaffListViewAdapter;

/**
 * Displays a list of all the members of {@link com.leocaretech.api.Staff} on the system.
 *
 * @author Josh Wright
 */
public class StaffFragment extends Fragment {

    /**
     * SharedPreferences object to allow data to be stored on the device.
     */
    SharedPreferences preferences = null;

    /**
     * SharedPreferences.Editor object to allow us to edit and update data stored on the device.
     */
    SharedPreferences.Editor editor = null;

    /**
     * Empty default constructor required for fragments.
     */
    public StaffFragment(){}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_staff, container, false);

        this.preferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
        this.editor = preferences.edit();

        // set up listview
        this.setupListView(rootView);

        return rootView;
    }

    /**
     * Set up the list view.
     *
     * @param rootView context of the rootview
     */
    private void setupListView(final View rootView){

        ListView staffListView = (ListView) rootView.findViewById(R.id.lvStaff);

        staffListView.setAdapter(new StaffListViewAdapter(getActivity()));

        staffListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                editor.putInt("staffID", position);
                editor.commit();

                Intent i = new Intent(getActivity(), StaffDetailActivity.class);
                startActivity(i);

            }
        });


    }

    /**
     * This method is called on resume to the activity, removes the previous
     * {@link com.leocaretech.api.Staff} id from memory ready for the next selection.
     */
    @Override
    public void onResume() {
        super.onResume();
        editor.remove("staffID");
        editor.commit();
    }
}
