package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.ServiceUser;
import com.leocaretech.leocare.R;

import java.util.ArrayList;


public class ServiceUserListViewAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Database db = null;


    public ServiceUserListViewAdapter(Context context){

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new Database("", "", context);

    }

    @Override
    public int getCount() {
        return db.getServiceUsers().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_clients_cell, null);

        ArrayList<ServiceUser> sus = db.getServiceUsers();

        TextView tvName = (TextView) rowView.findViewById(R.id.tvServiceUsersListViewName);
        TextView tvLocation = (TextView) rowView.findViewById(R.id.tvServiceUsersListViewLocation);

        ImageView ivImage = (ImageView) rowView.findViewById(R.id.ivServiceUsersListViewImage);

        tvName.setText(sus.get(position).getName());
        tvLocation.setText(sus.get(position).getAddress());

        ivImage.setImageDrawable(sus.get(position).getImage());


        return rowView;
    }
}
