package com.leocaretech.leocare.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leocaretech.leocare.R;

/**
 * Enables {@link com.leocaretech.api.Staff} users to view and edit their personal information.
 *
 * @author Josh Wright
 */
public class MeFragment extends Fragment {

    /**
     * Empty default constructor required for fragments.
     */
    public MeFragment(){}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_me, container, false);

        return rootView;
    }
}
