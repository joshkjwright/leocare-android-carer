package com.leocaretech.leocare.fragments.service_user_fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.ServiceUser;
import com.leocaretech.leocare.R;

/**
 * Displays all profile details relevant to a specific
 * {@link com.leocaretech.api.ServiceUser}.
 *
 * @author Josh Wright
 */
public class ServiceUserMyLifeFragment extends Fragment {

    /**
     * Empty default constructor required for fragments.
     */
    public ServiceUserMyLifeFragment() {}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Initialise the fields
        View rootView = inflater.inflate(R.layout.fragment_service_user_my_life, container, false);

        TextView tvName = (TextView) rootView.findViewById(R.id.tvServiceUserName);
        ImageView ivImage = (ImageView) rootView.findViewById(R.id.ivServiceUserImage);

        TextView tvOutcome = (TextView) rootView.findViewById(R.id.tvServiceUserOutcome);
        TextView tvMyLife = (TextView) rootView.findViewById(R.id.tvServiceUserMyLife);
        TextView tvOtherDetails = (TextView) rootView.findViewById(R.id.tvServiceUserOtherDetails);

        // Get the service user
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Database db = new Database("x", "y", rootView.getContext());

        ServiceUser su = db.getServiceUsers().get(preferences.getInt("serviceUserID", 0));

        // Set the values
        tvName.setText(su.getName());
        ivImage.setImageDrawable(su.getImage());

        tvOutcome.setText(su.getOutcome());
        tvMyLife.setText(su.getMyLife());
        tvOtherDetails.setText(su.getOtherDetails());

        return rootView;
    }

}
