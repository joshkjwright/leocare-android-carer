package com.leocaretech.leocare.fragments.service_user_fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.leocaretech.api.Database;
import com.leocaretech.api.Medication;
import com.leocaretech.api.ServiceUser;
import com.leocaretech.leocare.R;
import com.leocaretech.leocare.list_adapters.ServiceUserMedicationListViewAdapter;

/**
 * Displays all {@link com.leocaretech.api.Medication]s relevant to a specific
 * {@link com.leocaretech.api.ServiceUser}.
 *
 * @author Josh Wright
 */
public class ServiceUserMedicationFragment extends Fragment implements AdapterView.OnItemClickListener{

    /**
     * Empty default constructor required for fragments.
     */
    public ServiceUserMedicationFragment() {
    }

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_user_medication, container, false);

        // set up views
        this.setupListView(rootView);
        setHasOptionsMenu(true);

        return rootView;
    }

    /**
     * Set up views
     * @param rootView context of the rootview
     */
    private void setupListView(View rootView) {

        // ste up listview
        ListView lvMedication = (ListView) rootView.findViewById(R.id.lvServiceUserMedication);
        lvMedication.setAdapter(new ServiceUserMedicationListViewAdapter(getActivity()));
        lvMedication.setOnItemClickListener(this);
    }

    /**
     * Handles on item click events from the listview of medication.
     *
     * @param adapterView parent view
     * @param view current view
     * @param i position in the list view
     * @param l the row id of the item that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        // get shared prefs
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(view.getContext());
        Database db = new Database("x", "y", view.getContext());

        // get the user and medication
        ServiceUser su = db.getServiceUsers().get(preferences.getInt("serviceUserID", 0));
        Medication m = db.getServiceUsers().get(preferences.getInt("serviceUserID", 0)).
                getMedications().get(i);

        // temporary alert dialog to display details regarding medication whilst design is being
        // built.
        AlertDialog.Builder builder1 = new AlertDialog.Builder(view.getContext());
        builder1.setMessage(m.getName() + "\n" + m.getDetails() + "\n" + su.getName() +
                " is currently taking this medicine.");
        builder1.setCancelable(true);

        builder1.setNeutralButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder1.create();
        alert.show();
    }
}