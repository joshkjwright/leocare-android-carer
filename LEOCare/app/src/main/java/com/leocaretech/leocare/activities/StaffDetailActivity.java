package com.leocaretech.leocare.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.leocare.R;

/**
 * Displays all information regarding the selected staff member.
 *
 * @author Josh Wright
 */
public class StaffDetailActivity extends AppCompatActivity {

    /**
     * TextView containing the staff member's name
     */
    private TextView tvStaffName = null;

    /**
     * ImageView containing the staff member's image
     */
    private ImageView ivStaffImage = null;

    /**
     * TextView containing the staff member's mobile number
     */
    private TextView tvStaffNumber = null;

    /**
     * TextView containing the staff member's personal details
     */
    private TextView tvStaffDetails = null;

    /**
     * On create method responsible for the activity creation and the set up of the fields.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_detail);

        // Set up fields
        this.setupFields();
    }

    /**
     * Set up fields.
     */
    private void setupFields(){

        // connect to shared preferences and the database to get the information on the correct
        // staff member.
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Database db = new Database("", "", this);
        Staff staff = db.getStaff().get(preferences.getInt("staffID", 0));

        // instantiate fields
        this.tvStaffName = (TextView) findViewById(R.id.tvStaffDetailName);
        this.ivStaffImage = (ImageView) findViewById(R.id.ivStaffDetailImage);
        this.tvStaffNumber = (TextView) findViewById(R.id.tvStaffDetailPhoneNumber);
        this.tvStaffDetails = (TextView) findViewById(R.id.tvStaffDetailDetails);

        // set the values
        this.tvStaffName.setText(staff.getName());
        this.ivStaffImage.setImageDrawable(staff.getImage());
        this.tvStaffNumber.setText(staff.getNumber());
        this.tvStaffDetails.setText(staff.getDetails());

    }
}
