package com.leocaretech.leocare;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.ByteArrayOutputStream;

/**
 * Handles the capture of the signature within the
 * {@link com.leocaretech.leocare.activities.CompleteVisitActivity} Activity.
 *
 * @author Josh Wright
 */
public class CaptureSignatureView extends View {

    /**
     * Bitmap for the drawing.
     */
    private Bitmap bitmap;

    /**
     * Canvas to draw on.
     */
    private Canvas canvas;

    /**
     * Contains the path of the drawing.
     */
    private Path path;

    /**
     * The paint object to be used on with the bitmap.
     */
    private Paint bitmapPaint;

    /**
     * Allows you to paint onto the canvas
     */
    private Paint paint;

    /**
     * X dimension of the point when canvas is touched
     */
    private float mX;

    /**
     * Y dimension of the point when canvas is touched
     */
    private float mY;

    /**
     * Constructor used to create the canvas and set up the fields to use within.
     * @param context parent context
     * @param attr set of attributes to use
     */
    public CaptureSignatureView(Context context, AttributeSet attr) {
        super(context, attr);
        path = new Path();
        bitmapPaint = new Paint(Paint.DITHER_FLAG);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.argb(255, 0, 0, 0));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(4);
    }

    /**
     * Handles the resizing of the canvas.
     *
     * @param w     new width
     * @param h     new height
     * @param oldw  old width
     * @param oldh  old height
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.bitmap = Bitmap.createBitmap(w, (h > 0 ? h : ((View) this.getParent()).getHeight()),
                Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(this.bitmap);
    }

    /**
     * Handles all on draw events from within this class.
     *
     * @param canvas canvas on which the user is drawing on.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, bitmapPaint);
        canvas.drawPath(path, paint);
    }

    /**
     * This method handles the touch on the canvas.
     *
     * @param x x coordinates of touch
     * @param y y coordinates of touch
     */
    private void TouchStart(float x, float y) {
        path.reset();
        path.moveTo(x, y);
        mX = x;
        mY = y;
    }

    /**
     * Handles the move of the touch whilst the finger is still touched down.
     *
     * @param x x coordinates of moved finger
     * @param y y coordinates of moved finer
     */
    private void TouchMove(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (dx >= 4 || dy >= 4) {
            path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    /**
     * Handles the release of a touch on the canvas i.e. when a user takes their finger off the
     * canvas.
     */
    private void TouchUp() {
        if (!path.isEmpty()) {
            path.lineTo(mX, mY);
            canvas.drawPath(path, paint);
        } else {
            canvas.drawPoint(mX, mY, paint);
        }

        path.reset();
    }

    /**
     * Handles all touch events on the canvas and calls private methods within this class.
     *
     * @param e type of event
     * @return true/false depending on if the event has been handled.
     */
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        super.onTouchEvent(e);
        float x = e.getX();
        float y = e.getY();

        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                TouchStart(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                TouchMove(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                TouchUp();
                invalidate();
                break;
        }

        return true;
    }

    /**
     * Clears the canvas
     */
    public void ClearCanvas() {
        canvas.drawColor(Color.WHITE);
        invalidate();
    }

    /**
     * Get the byte array of data from the canvas in order to store and send to the server.
     *
     * @return byte array of the signature data
     */
    public byte[] getBytes() {
        Bitmap b = getBitmap();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /**
     * Returns the bitmap in order to save as a byte array and send to the server.
     *
     * @return bitmap of image
     */
    public Bitmap getBitmap() {
        View v = (View) this.getParent();
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);

        return b;
    }
}