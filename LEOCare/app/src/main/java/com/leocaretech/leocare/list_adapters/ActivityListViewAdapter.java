package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leocaretech.api.Activity;
import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.api.Visit;
import com.leocaretech.leocare.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Handles the creation of a custom listview containing {@link Activity}s.
 *
 * @author Josh Wright
 */
public class ActivityListViewAdapter extends BaseAdapter {

    /**
     * Inflater to inflate the listview.
     */
    private static LayoutInflater inflater = null;

    /**
     * SharedPreferences object to allow data to be stored on the device.
     */
    private SharedPreferences preferences = null;

    /**
     * Database object to allow safe access to the database.
     */
    private Database db = null;

    /**
     * Inflates the listview using the required context parameter.
     *
     * @param context context of parent class
     */
    public ActivityListViewAdapter(Context context){

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        db = new Database("x", "y", context);
    }

    /**
     * Returns the total count of listview cells.
     *
     * @return amount of cells in the custom listview
     */
    @Override
    public int getCount() {
        return db.getVisits(db.getStaff().get(0)).get(preferences.getInt("visitID", 0)).getActivities().size();
    }

    /**
     * Get item at a specific position.
     *
     * @param position position of item
     * @return position of item
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Returns the id of the requested item.
     *
     * @param position position of the request item
     * @return position of the item
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Generates the custom listview cell.
     *
     * @param position position of the cell in the listview
     * @param convertView view to convert into new view
     * @param parent parent view
     * @return newly created view to insert into the custom listview
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_activities_cell, null);

        // get fields
        ImageView image = (ImageView) rowView.findViewById(R.id.imageTypeOfActivity);
        TextView text = (TextView) rowView.findViewById(R.id.textTypeOfActivity);
        ImageView imgTick = (ImageView) rowView.findViewById(R.id.imgActivityTick);

        // pull information regarding visit
        Visit visit = db.getVisits(db.getStaff().get(0)).get(preferences.getInt("visitID", 0));

        // pull information about the specific activity, relating to the position in the list
        Activity activity = visit.getActivities().get(position);

        // set the fields
        image.setImageDrawable(activity.getImage());
        text.setText(activity.getTitle());

        // set 'done' tick if activity has been completed
        imgTick.setVisibility(preferences.getBoolean(Integer.toString(position), false)
                ? View.VISIBLE : View.GONE);

        // return new view
        return rowView;
    }
}
