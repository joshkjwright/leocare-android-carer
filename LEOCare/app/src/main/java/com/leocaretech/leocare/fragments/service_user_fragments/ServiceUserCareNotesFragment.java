package com.leocaretech.leocare.fragments.service_user_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.leocaretech.leocare.R;
import com.leocaretech.leocare.list_adapters.ServiceUserCareNotesListViewAdapter;

/**
 * Displays all previous {@link com.leocaretech.api.Visit}s and care notes for a specific
 * {@link com.leocaretech.api.ServiceUser}.
 *
 * @author Josh Wright
 */
public class ServiceUserCareNotesFragment extends Fragment {

    /**
     * Empty default constructor required for fragments.
     */
    public ServiceUserCareNotesFragment(){}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflate the view
     * @param container containing to contain the fragment
     * @param savedInstanceState
     * @return the newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_user_care_notes, container, false);

        // set up fields within fragment using the rootview context.
        this.setupListView(rootView);

        return rootView;
    }

    /**
     * Set up the fragment views.
     *
     * @param rootView context of the rootview
     */
    private void setupListView(View rootView){

        ListView lvCareNotes = (ListView) rootView.findViewById(R.id.lvServiceUserCareNotes);
        lvCareNotes.setAdapter(new ServiceUserCareNotesListViewAdapter(getActivity()));
        lvCareNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // TODO - launch care notes detail activity
/*
                Intent i = new Intent(getActivity(), ServiceUserCareNotesDetailActivity.class);
                startActivity(i);
*/
            }
        });
    }
}