package com.leocaretech.leocare.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.leocaretech.api.Database;
import com.leocaretech.leocare.R;
import com.leocaretech.leocare.activities.StartVisitActivity;
import com.leocaretech.leocare.list_adapters.VisitListViewAdapter;

/**
 * Enables {@link com.leocaretech.api.Staff} users to view their upcoming
 * {@link com.leocaretech.api.Visit}s.
 *
 * @author Josh Wright
 */
public class VisitsFragment extends Fragment implements AdapterView.OnItemClickListener{

    /**
     * SharedPreferences object to allow data to be stored on the device.
     */
    private SharedPreferences preferences = null;

    /**
     * Database object to allow safe access to the database.
     */
    private Database db = null;

    /**
     * Empty default constructor required for fragments.
     */
    public VisitsFragment() {}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_tab_host, container, false);

        // get shared prefs
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        db = new Database("x", "y", rootView.getContext());

        // set up list view
        this.setupListView(rootView);

        return rootView;
    }

    /**
     * Set up listview.
     *
     * @param rootView context of rootview
     */
    private void setupListView(View rootView){

        ListView listViewVisits = (ListView) rootView.findViewById(R.id.listViewVisits);
        listViewVisits.setAdapter(new VisitListViewAdapter(getActivity()));
        listViewVisits.setOnItemClickListener(this);
    }

    /**
     * Handles on item click events from the listview of visits.
     *
     * @param adapterView parent view
     * @param view current view
     * @param position position in the list view
     * @param l the row id of the item that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        SharedPreferences.Editor editor = preferences.edit();

        // clear all previous data regarding a visit
        for (int i = 0; i <= db.getVisits(db.getStaff().get(0)).get(position).getActivities().size(); i++){
            editor.putBoolean(Integer.toString(i), false);
        }

        editor.putBoolean("complete", false);

        // store visit ID in system memory
        editor.putInt("visitID", position);
        editor.apply();

        // start visit
        Intent i = new Intent(getActivity(), StartVisitActivity.class);
        startActivity(i);

    }
}