package com.leocaretech.leocare.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.leocaretech.api.Activity;
import com.leocaretech.api.Database;
import com.leocaretech.api.Staff;
import com.leocaretech.leocare.R;

/**
 * Displays all details regarding an {@link Activity}.
 *
 * @author Josh Wright
 */
public class ActivityDetailActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Button to complete the activity.
     */
    private Button bComplete = null;

    /**
     * Button to set the activity as incomplete.
     */
    private Button bIncomplete = null;

    /**
     * Database object to allow access to the database.
     */
    private Database db = null;

    /**
     * This method is ran on creation of this class and sets up the screen.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity);

        // Initialise the database
        db = new Database("x", "y", this);

        // Set up all fields on the screen
        setupFields();
    }

    /**
     * This method sets up all required fields.
     */
    private void setupFields(){

        this.bComplete = (Button) findViewById(R.id.bComplete);
        this.bIncomplete = (Button) findViewById(R.id.bNotComplete);

        bComplete.setOnClickListener(this);
        bIncomplete.setOnClickListener(this);
    }

    /**
     * Handles all click events on the screen.
     *
     * @param v view from which the click event originated from
     */
    @Override
    public void onClick(View v) {

        switch(v.getId()){

            // If 'Complete' was clicked
            case R.id.bComplete:

                // Get position
                Bundle extras = getIntent().getExtras();
                String pos = Integer.toString(extras.getInt("pos"));

                // Get sharedprefs
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = preferences.edit();

                // Store this activityID and note its completion
                editor.putBoolean(pos, true);

                // Get total of activities to check if final activity
                String totalActivities = Integer.toString(db.getVisits(db.getStaff().get(0)).get(preferences.getInt("VisitID", 0)).getActivities().size() - 1);

                // If it is the final activity, store this in memory
                if (pos.equalsIgnoreCase(totalActivities)){
                    // TODO - check if all activities have been completed.
                    editor.putBoolean("complete", true);
                }

                // Save data
                editor.apply();

                // Return to previous activity
                Intent i = new Intent(this, VisitDetailActivity.class);
                startActivity(i);


                break;

            // If 'Incomplete was clicked
            case R.id.bNotComplete:

            default:

                // Display 'Activity not completed' screen
                i = new Intent(this, ActivityNotCompleteActivity.class);
                startActivity(i);

                break;
        }

    }
}
