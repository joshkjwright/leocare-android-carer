package com.leocaretech.leocare.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.leocaretech.api.Database;
import com.leocaretech.leocare.activities.ServiceUserDetailActivity;
import com.leocaretech.leocare.list_adapters.ServiceUserListViewAdapter;
import com.leocaretech.leocare.R;

/**
 * Displays a list of all {@link com.leocaretech.api.ServiceUser}s on the system.
 *
 * @author Josh Wright
 */
public class ServiceUsersFragment extends Fragment {

    /**
     * Empty default constructor required for fragments.
     */
    public ServiceUsersFragment(){}

    /**
     * Handles the creation of the fragment and the set up of all fields within.
     *
     * @param inflater inflates the fragment
     * @param container contains the fragment
     * @param savedInstanceState
     * @return newly created view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_clients, container, false);

        ListView listViewClients = (ListView) rootView.findViewById(R.id.listViewClients);
        listViewClients.setAdapter(new ServiceUserListViewAdapter(getActivity()));

        // set on item click listener
        listViewClients.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("serviceUserID", position);
                editor.commit();

                // launch service user detail activity
                Intent i = new Intent(getActivity(), ServiceUserDetailActivity.class);
                startActivity(i);
            }
        });
        return rootView;
    }

    /**
     * This method is called on resume to the activity, removes the previous
     * {@link com.leocaretech.api.ServiceUser} id from memory ready for the next selection.
     */
    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("serviceUserID");
        editor.commit();
    }
}
