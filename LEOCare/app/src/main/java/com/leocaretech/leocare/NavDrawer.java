package com.leocaretech.leocare;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.leocaretech.leocare.fragments.ChatFragment;
import com.leocaretech.leocare.fragments.ServiceUsersFragment;
import com.leocaretech.leocare.fragments.MeFragment;
import com.leocaretech.leocare.fragments.StaffFragment;
import com.leocaretech.leocare.fragments.VisitsFragment;

/**
 * Handles all navigation through the application.
 *
 * @author Josh Wright
 */
public class NavDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * On create method called upon creation of the activity, handles setting up the
     * {@link NavDrawer}.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Show visits fragment first
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_nav_drawer, new VisitsFragment())
                .commit();


        // get details about user thats logged in
        Boolean isManager = preferences.getBoolean("isManager", false);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        // if the user is a manager show the staff section, otherwise hide it.
        if (!isManager)
            nav_Menu.findItem(R.id.nav_staff).setVisible(false);
    }

    /**
     * Overrides the native soft touch back button pressed functionality to check if navigation
     * drawer is open once the back button is pressed.
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Displays different fragment depending on which section is selected in the navigation view.
     *
     * @param item item selected from the nav drawer
     * @return true/false if its been handled
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        Fragment fragment = null;

        switch(item.getItemId()){
            case R.id.nav_visits:
                fragment = new VisitsFragment();
                setTitle("My Visits");
                break;
            case R.id.nav_staff:
                fragment = new StaffFragment();
                setTitle("My Staff");
                break;
            case R.id.nav_chat:
                fragment = new ChatFragment();
                setTitle("Chat");
                break;
            case R.id.nav_clients:
                fragment = new ServiceUsersFragment();
                setTitle("My Service Users");
                break;
            case R.id.nav_me:
                fragment = new MeFragment();
                setTitle("Me");
                break;
        }


        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_nav_drawer, fragment)
                .commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
