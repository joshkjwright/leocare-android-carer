package com.leocaretech.leocare.list_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leocaretech.api.Activity;
import com.leocaretech.leocare.R;

/**
 * Handles the creation of a custom listview containing {@link com.leocaretech.api.ISPForm}s.
 *
 * @author Josh Wright
 */
public class ServiceUserISPListViewAdapter extends BaseAdapter {

    /**
     * Inflater to inflate the listview.
     */
    private static LayoutInflater inflater = null;

    /**
     * Inflates the listview using the required context parameter.
     *
     * @param context context of parent class
     */
    public ServiceUserISPListViewAdapter(Context context){

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Returns the total count of listview cells.
     *
     * @return amount of cells in the custom listview
     */
    @Override
    public int getCount() {
        return 8;
    }

    /**
     * Get item at a specific position.
     *
     * @param position position of item
     * @return position of item
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Returns the id of the requested item.
     *
     * @param position position of the request item
     * @return position of the item
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Generates the custom listview cell.
     *
     * @param position position of the cell in the listview
     * @param convertView view to convert into new view
     * @param parent parent view
     * @return newly created view to insert into the custom listview
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = inflater.inflate(R.layout.listview_service_user_isp_cell, null);

        TextView name = (TextView) rowView.findViewById(R.id.tvServiceUserISPCellName);


        return rowView;
    }
}
